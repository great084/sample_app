module SessionsHelper
  
  #渡されたユーザでログインする
  def log_in(user)
    session[:user_id] = user.id
  end
  
  # ユーザのセッションを永続的にする
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end
  
  #渡されたユーザがログイン中のユーザであればtrue
  def current_user?(user)
    user == current_user
  end
  
  #現在ログイン中のユーザを帰す
  def current_user
    # sessionにユーザidが存在する場合
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    # sessionにユーザidが存在しない場合で、cookiesに存在する場合
    elsif (user_id = cookies.signed[:user_id])
      #raise  #testでpathの確認
      user = User.find_by(id: user_id)
      if user  && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end
  
  #ユーザがログインしていればtrue,そうでなければfalse
  def logged_in?
    !current_user.nil?
  end
  
  #現在のユーザをログアウトする
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end
  
  # 永続的セッションを破棄する
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end
  
  # 記憶したURL(もしくはデフォルト)にリダイレクト
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # アクセスしようとしたURLを記憶
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
    #puts "forwarding_url= " + session[:forwarding_url]
  end
end
