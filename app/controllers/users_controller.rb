class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy,
                                        :following, :followers]
  before_action :collect_user, only: [:edit, :update]
  before_action :admin_user, only: [:destroy]

  def index
    @users = User.where(activated: true).paginate(page: params[:page])
  end
  
  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
    redirect_to root_url unless @user.activated?
    #debugger
  end
  
  def new
    @user = User.new
    #debugger
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_mail
      #UserMailer.account_activation(@user).deliver_now # 左記の処理はuser.rbへ移動
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
      #log_in @user
      #flash[:success] = "Welcome to the Sample Page"
      #redirect_to @user  #redirect_to user_url(@user)の略
    else
      render 'new'
    end
  end
  
  def edit
    #@user = User.find(params[:id])
  end  

  def update
    #@user = User.find(params[:id])
    if @user.update_attributes(user_params)
      #成功した場合の更新
      flash[:success] = "Profile updated "
      redirect_to @user  #redirect_to user_url(@user)の略
    else
      render 'edit'
    end
  end  
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "user deleted"
    redirect_to users_path
  end
  
  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end
  
  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  
  private
  
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                  :password_confirmation)
    end
    
    # # ログイン済みユーザか確認 →  application controllerへ移動
    # def logged_in_user
    #   unless logged_in?
    #     store_location
    #     flash[:danger] = "Please log in."
    #     redirect_to login_path
    #   end
    # end
    
    #正しいユーザか確認
    def collect_user
      @user = User.find(params[:id])
      #puts "collect_user check is"
      #puts current_user?(@user)
      redirect_to(root_url) unless current_user?(@user)
    end
    
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
