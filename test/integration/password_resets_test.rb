require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest
  
  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:michael)
  end
  
  test "password reset" do
    get new_password_reset_path
    assert_template 'password_resets/new'
    # メールアドレスが無効
    post password_resets_path, params: { password_reset: {email: ""}}
    assert_template 'password_resets/new'
    assert_not flash.empty?
    assert_select 'div.alert-danger'
    # メールアドレスが有効
    post password_resets_path, params:{ password_reset: {email: @user.email} }
    assert_not_equal @user.reset_digest, @user.reload.reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_redirected_to root_url
    # puts assigns(:user)
    # follow_redirect!
    # puts assigns(:user)
    assert_not flash.empty?
    # パスワード再設定のテスト
    user = assigns(:user)
    # メールアドレスが無効
    get edit_password_reset_path(user.reset_token, email: "")
    assert_redirected_to root_url
    # 無効なユーザ
    user.toggle!(:activated)
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_redirected_to root_url
    user.toggle!(:activated)
    # メールアドレスが有効でトークンが無効
    get edit_password_reset_path("invalid_token", email: user.email)
    assert_redirected_to root_url
    # メールアドレスもトークンも有効
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_template 'password_resets/edit'
    assert_select 'input[name=email][type=hidden][value=?]', user.email
    # 無効なパスワードとパスワード確認
    patch password_reset_path(user.reset_token),
          params: { email: user.email,
                    user: { password: "foobar",
                            password_confirmation: "bazbaz"}}
    assert_select 'div#error_explanation'
    # パスワードが空
    patch password_reset_path(user.reset_token),
          params: { email: user.email,
                    user: { password: "",
                            password_confirmation: "" } }
    assert_select 'div#error_explanation'
    # 有効なパスワードとパスワード確認
    patch password_reset_path(user.reset_token),
          params: { email: user.email,
                    user: { password: "foobaz",
                            password_confirmation: "foobaz" } }
    assert is_logged_in?
    assert_nil @user.reload.reset_digest
    assert_not flash.empty?
    assert_redirected_to user
  end
  
  test "expired reset token" do
    get new_password_reset_path
    post password_resets_path, params: { password_reset: { email: @user.email } }
    @user = assigns(:user)
    get edit_password_reset_path(@user.reset_token, email: @user.email)
    @user.update_attribute(:reset_sent_at, 3.hours.ago)
    patch password_reset_path(@user.reset_token), 
          params: { email: @user.email,
                    user: { password: "foobaz",
                            password_confirmation: "foobaz" } }
    assert_response :redirect
    assert_redirected_to new_password_reset_url
    follow_redirect!
    assert_match /.*reset.*expired.*/i, response.body
    assert_select 'div.alert', 'Password reset has expired' 
  end
end
