require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:michael)
    @non_admin = users(:archer)
    @activated = users(:archer)
    @non_activated = users(:ghost)
  end
  
  test "index includes only activated user" do
    log_in_as(@activated)
    get users_path
    assert_template 'users/index'
    assert_select 'a',text: @non_activated.name, count:0
    assert_select 'a',text: @activated.name,     count:1
  end

  test "index as admin includes pagination" do
    log_in_as(@admin)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination', 2
    User.where(activated: true).paginate(page: 1).each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
      if user == @admin
        assert_select 'a[href=?]', user_path(user), text: 'delete', count:0
      else
        assert_select 'a[href=?]', user_path(user), text: 'delete'
      end
    end
    assert_difference "User.count", -1 do
      delete user_path(@non_admin)
    end
  end

  test "index as non-admin includes pagination" do
    log_in_as(@non_admin)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination', 2
    User.where(activated: true).paginate(page: 1).each do |user|
      #puts user.name
      assert_select 'a[href=?]', user_path(user), text: user.name
    end
    assert_select 'a', text:'delete', count:0
  end


end
