require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end
  
  test 'invalid login should be failed' do
    get login_path
    assert_template 'sessions/new'
    post login_path, params: {session: { email: 'foo@bar.com',
                                         password: '111112' }}
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
  
  test 'login with valid information followed by logout' do
    #log in
    get login_path
    post login_path, params: {session: { email: @user.email,
                                         password: 'password' }}
    assert is_logged_in?
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count:0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    assert_select "strong.stat", count:2
    # logout
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    # 別ブラウザなどで再度logoutした場合を想定
    delete logout_path
    follow_redirect!
    assert_template 'static_pages/home'
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count:0
    assert_select "a[href=?]", user_path(@user), count:0
  end
  
  test "login with remember_me" do
    log_in_as(@user, remember_me: '1')
    assert_not_empty cookies['remember_token']
    assert_equal cookies['remember_token'], assigns(:user).remember_token
  end
  
  test "login without remember_me" do
    log_in_as(@user, remember_me: '1')
    delete logout_path
    log_in_as(@user, remember_me: '0')
    # puts cookies['remember_token']
    assert_empty cookies['remember_token']
  end
  
  
end
