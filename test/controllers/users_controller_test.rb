require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
    @other_user = users(:archer)
    @non_activated = users(:ghost)
  end
  
  test "should get new" do
    get signup_path
    assert_response :success
  end

  test "shold redirect edit  when not login" do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_path
  end
  
  test "shold redirect update  when not login" do
    patch user_path(@user), params: { user: { name: @user.name,
                                              email: @user.email }}    
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test "should  render edit when login" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert flash.empty?
    assert_template 'users/edit'
  end
  
  test "should redirect edit when wrong user login" do
    log_in_as(@other_user)
    get edit_user_path(@user)
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "shold redirect update  when wrong user login" do
    log_in_as(@other_user)
    patch user_path(@user), params: { user: { name: @user.name,
                                              email: @user.email }}    
    assert flash.empty?
    assert_redirected_to root_url
  end
  
  test "should redirect index when user not logedin" do
    get users_path
    assert_redirected_to login_path
  end
  
  test "should not allow the admin attribute to be edited via web" do
    log_in_as(@other_user)
    assert_not @other_user.admin?
    patch user_path(@other_user), params: {user: {password: "foobar",
                                                  password_confirmation: "foobar",
                                                  admin: "true"}}
    assert_not @other_user.reload.admin?
  end
  
  test "should redirect delete when not logged in" do
    assert_no_difference 'User.count' do
      delete user_path(@other_user)
    end
    assert_redirected_to login_path
  end
  
  test "shoule redirect delete when logged in as non-admin user" do
    log_in_as(@other_user)
    # puts @other_user
    assert_no_difference "User.count" do
      delete user_path(@user)
    end
    assert_redirected_to root_url
  end
  
  test "shoulde delete user when logged in as admin user" do
    log_in_as(@user)
    assert_difference "User.count", -1 do
      delete user_path(@other_user)
    end
    assert_redirected_to users_path
    assert_not flash.empty?
  end
  
  test "should redirect to root when get non_activated user_path" do
    log_in_as(@user)
    get user_path(@non_activated)
    assert_redirected_to root_url
  end
  
  test "should show user when get activated user_path" do
    log_in_as(@user)
    get user_path(@other_user)
    assert_template 'users/show'
  end
  
  test "should redirect following page when not logged in" do
    get following_user_path(@user)
    assert_redirected_to login_url
  end

  test "should redirect followers page when not logged in" do
    get followers_user_path(@user)
    assert_redirected_to login_url
  end
end
