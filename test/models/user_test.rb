require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(name: "Example User", email: "user@example.com",
                    password: "foobar", password_confirmation: "foobar")
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "name should be presence" do
    @user.name = "  "
    assert_not @user.valid?
  end

  test "email should be presence" do
    @user.email = "    "
    assert_not @user.valid?
  end
  
  test "name should not be longer than 50" do
    @user.name  = "a" * 51
    assert_not @user.valid?
  end
  
  test "email should not be longer than 255" do
    @user.email  = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end
  
  test "email validation should accept valid address" do
    valid_addresses = %w[user@example.com User@foo.COM A_US-ER@foo.bar.org
                      first.last@foo.jp alice++bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com User_at_foo.COM user.name@example.
                      foo@bar_baz.com foo@baz+baz.com foo@bar..com]
    invalid_addresses.each do |invalid_address|
      #puts invalid_address
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
  test "email addresses should be unique" do
    dupulicate_user = @user.dup
    dupulicate_user.email = dupulicate_user.email.upcase
    #puts dupulicate_user.email
    @user.save
    assert_not dupulicate_user.valid?
  end
  
  test "email address shoul be saved as lower-case" do
    mixed_case_email = "Foo@Example.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end

  test "password should be a present (non-blanke" do
    @user.password = @user.password_confirmation = " "*6
    assert_not @user.valid?
  end
  
  test "password should be have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false for a user without digest" do
    assert_not @user.authenticated?(:remember, '')
  end

  test "associated microposts should be destroyed when user deleted " do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference "Micropost.count", -1 do
      @user.destroy
    end
  end
  
  test "should follow and unfollow a user" do
    michael = users(:michael)
    archer  = users(:archer)
    assert_not michael.following?(archer)
    michael.follow(archer)
    assert michael.following?(archer)
    assert archer.followers.include?(michael)
    michael.unfollow(archer)
    assert_not michael.following?(archer)
  end
  
  test "feed should have the right posts" do
    michael = users(:michael)
    archer  = users(:archer)
    lana    = users(:lana)
    # 自分自身の投稿を確認
    michael.microposts.each do |post_following|
      assert michael.feed.include?(post_following)
    end
    # フォローしているユーザの投稿を確認
    lana.microposts.each do |post_following|
      assert michael.feed.include?(post_following)
    end
    # フォローしていないユーザの投稿を確認
    archer.microposts.each do |post_following|
      assert_not michael.feed.include?(post_following)
    end
  end

end
